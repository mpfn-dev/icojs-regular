import util from "@ng-mpfn/icon-util/util.js";

const meta = util.getMeta("regular");

let iconName
const lines = meta.map((icon) => {
  iconName = `i-${icon.name}`
  iconName = iconName.replace(/(\-\w)/g, str => str[1].toUpperCase())
  return `export const ${iconName}: object = {viewBox:"${icon.viewbox}",path:"${icon.path}"};`;
});

const output = lines.join("\n");

util.write("icons.ts", output);
